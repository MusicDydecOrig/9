"use strict";

let numberOfFilms;

function start() {
    numberOfFilms = +prompt("Сколько фильмов вы уже посмотрели?", "");
    while (numberOfFilms == '' || numberOfFilms == null || isNaN(numberOfFilms)) {
        numberOfFilms = +prompt("Сколько фильмов вы уже посмотрели?", "");
    }
}

start ();

const personalMovieDB = {
    count : numberOfFilms,
    movies: {},
    actors: '',
    genres: [],
    privat: false
};

function rememberMyFilms(){
  for (let i = 0; i < 2; i++) {
    const vop1 = prompt("Один из последних просмотренных фильмов?", ""),
          vop2 = prompt("На сколько оцените его?", "");

      if (vop1 != null && vop2 != null && vop1 != '' && vop2 != '' && vop1.length < 50) {
          personalMovieDB.movies[vop1] = vop2;
      } else {
          alert('Ответы так себе');
          i--;
      }

  }
}

rememberMyFilms();

function detectPersonalLevel(){
  if (personalMovieDB.count < 10) {
    alert('Просмотрено довольно мало фильмов');
  } else if (personalMovieDB.count >= 10 && personalMovieDB.count <= 30) {
    alert('Вы классический зритель');
  } else if (personalMovieDB.count == 30) {
    alert('Вы киноман.');
  } else {
    alert('Произошла ошибка');
  }
}

detectPersonalLevel();

function showMyDB(hidden) {
  if (!hidden) {
      console.log(personalMovieDB);
  }
}

showMyDB(personalMovieDB.privat);

function writeYourGenres() {
  for (let i = 0; i<=2; i++) {
      const genre = prompt(`Ваш любимый жанр под номером ${i+1}`);
      personalMovieDB.genres[i] = genre;
  }
}

writeYourGenres();
